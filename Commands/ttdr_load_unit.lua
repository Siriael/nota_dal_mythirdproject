function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "rescueunits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
			
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition


function Run(self, units, parameter)
 	local atlasGroup = parameter.atlas
	local listOfUnitsToRescue = parameter.rescueunits
	--local WantedPosition = parameter.position
	local thisUnitID
    local cmdID = CMD.LOAD_UNITS
	local radius = 50
		for posIndex, unitID in pairs(atlasGroup) do
			thisUnitID  = unitID		
		end
		Spring.Echo (listOfUnitsToRescue)
		
		
		local posX, posY, posZ = SpringGetUnitPosition(listOfUnitsToRescue[1])
		if(Spring.ValidUnitID(listOfUnitsToRescue[1]) == nil)then
			return FAILURE
		end
		
		if(Spring.ValidUnitID(atlasGroup[1]) == nil)then
			return FAILURE
		end
	    --local posX, posY, posZ = SpringGetUnitPosition(listOfUnitsToRescue[1])
        --SpringGiveOrderToUnit(thisUnitID,    cmdID, {posX, posY, posZ, 0},  {}) --- load units into atlas 
		
	--	SpringGiveOrderToUnit(atlasUnits[i], cmdID, {posX, posY, posZ, 0},  {})
	--[[if (#Spring.GetUnitIsTransporting(thisUnitID) ~= 1) then
		return RUNNING
	else 
		return SUCCESS
	end
	--]]
		SpringGiveOrderToUnit(thisUnitID, cmdID, {posX,posY,posZ, radius},  {}) --- load units into bear 
	
		if (#Spring.GetUnitIsTransporting(thisUnitID) >= 1) then
			return SUCCESS
		else
			if Spring.ValidUnitID(listOfUnitsToRescue[1])then					
				Spring.Echo(#Spring.GetUnitIsTransporting(thisUnitID))
				return RUNNING
			else
				return FAILURE
			end
		end
	

    

end