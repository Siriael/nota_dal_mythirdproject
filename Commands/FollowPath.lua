function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end

local function ClearTable()
	local mytmpTable = bb[atlasID]
	mytmpTable.PathIndex = 1
	mytmpTable.UnitStucked = 0
	mytmpTable.StuckedTime = 0
	mytmpTable.OrderGiven = nil
	mytmpTable.path = nil
	bb[self.atlasID] = mytmpTable
	mytmpTable= nil

end

local function ClearState(self)
	
	mpath = nil
	atlasGroup = nil
	atlasID = nil
	pointX = nil
	pointY = nil
	pointZ = nil
	atlasPosition = nil
	dest = nil
	
end

local threshold =  300

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self,units,parameter)
	
	
	
	local atlasGroup = parameter.atlas
	local mpath = parameter.path
	local atlasID = atlasGroup[1]
	local myTable = {}
	local Skip = false
	--if(bb[atlasID] == nil)then
	--	myTable = { PathIndex = 1 , UnitStucked = 0 ,StuckedTime = 0 , OrderGiven = false , path = mpath }
	--	bb[atlasID] = myTable
	--else
	--	myTable = bb[atlasID]
	--end
	myTable = bb[atlasID]
	local finished = false
	if(myTable.PathIndex == nil)then
		myTable.PathIndex = 1
	end
	local cmdID = CMD.MOVE
	treshold = 300
	if(#myTable.path == 1 or myTable.PathIndex == #myTable.path-1 )then
			myTable.PathIndex = 1
			myTable.StuckedTime = 1
			bb[atlasID] = myTable
		return SUCCESS
	end
	local pointX, pointY, pointZ = SpringGetUnitPosition(atlasID)
	local atlasPosition = Vec3(pointX, pointY, pointZ)
	
	dest = myTable.path[myTable.PathIndex+1]	
	if(myTable.PathIndex == 1)then
		local x = atlasPosition.x + math.random(100)-50 
		local z = atlasPosition.z + math.random(100)-50 
		y = Spring.GetGroundHeight(x, z)
		dest = Vec3(x,y,z)
		end
	if(myTable.PreviousPosition == nil)then
		myTable.PreviousPosition = atlasPosition
	else
		if(myTable.PreviousPosition:Distance(atlasPosition) < 30 )then
			
			if (myTable.UnitStucked == nil) then
				myTable.UnitStucked = 1
			else
				
				myTable.UnitStucked = myTable.UnitStucked +1 
				if(myTable.UnitStucked > 5)then
					
					myTable.UnitStucked = 1
					if(myTable.StuckedTime == nil)then
					   myTable.StuckedTime = 1
					else
						myTable.StuckedTime = myTable.StuckedTime + 1
					end
					if(myTable.StuckedTime == 2 and myTable.PathIndex >= 2)then
						myTable.PathIndex = myTable.PathIndex - 1
					else
						if(myTable.StuckedTime == 3)then
							myTable.PathIndex = myTable.PathIndex + 1
						else
							if(myTable.StuckedTime == 4)then
								myTable.PathIndex = myTable.PathIndex + 1
							
							end
						end
					end
						local x = atlasPosition.x + math.random(200)-100 
						local z = atlasPosition.z + math.random(200)-100 
						y = Spring.GetGroundHeight(x, z)
						dest = Vec3(x,y,z)
						myTable.orderGiven = nil
						Skip = true
					if(myTable.StuckedTime >= 10)then
						return FAILURE
					end
					
				end
			end	
		else
			myTable.PreviousPosition = atlasPosition
			myTable.UnitStucked = 1
		--	myTable.StuckedTime = 1
		end
	end
	
	--while(finished ==false)do
	if(Skip == false)then
		if (atlasPosition:Distance(dest) < treshold) then
			if(myTable.PathIndex == #myTable.path-1) then
				finished = true
				myTable.PathIndex = 1
				myTable.StuckedTime = 1
				bb[atlasID] = myTable
			--	ClearTable()
				return SUCCESS
			else
				myTable.PathIndex = myTable.PathIndex + 1 
				dest = myTable.path[myTable.PathIndex+1]
				myTable.orderGiven = nil
			end
		end
	end
		if(Spring.ValidUnitID(atlasID) )then
			if(myTable.orderGiven == nil )then
					myTable.orderGiven = true
					SpringGiveOrderToUnit(atlasID, cmdID, dest:AsSpringVector(), {'shift'})
			end		
			bb[atlasID] = myTable
			return RUNNING								
		else
			bb[atlasID] = myTable
		--	ClearTable()
			return FAILURE
		end
	--end
	
	--return SUCCESS
	
end

function Reset(self)
	ClearState(self)
end



