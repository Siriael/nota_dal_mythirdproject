local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back





-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitIDs] unfiltered list 

-- @return groupArrays [table] filtered list
return function(listOfUnits, numberOfGroups)

local groupArray = {}
local i = 0
for index,UnitId in pairs (listOfUnits) do
	
					
					if(i>=numberOfGroups)then
						i = 1
					else
						i= i +1
					end
					
					
					local temp = groupArray[i]
					if(temp == nil)then
						temp = {}
						temp[1] = UnitId
					else
						temp[#temp + 1] = UnitId
					end
					
					groupArray[i] = temp

	end
	--[[
	
	
	local groupSize = math.floor(#listOfUnits / numberOfGroups)
	if(groupSize<1)then
		groupSize = 1
	end
	local groupArray = {}

	local j = 1
	local i = 1
	for j = 1, numberOfGroups do
		local tempGroup = {}
		local k = 1
		for  k = 1,groupSize do	
			tempGroup[#tempGroup + 1] = listOfUnits[i]
			--tempGroup[listOfUnits[k]]--[[ = i
			k = k+1
			i = i + 1 
		end
		groupArray[j] = tempGroup  
		j = j +1 
	end
		--[[
		local tempGroup = {}
		local k = 1
		for i = 1,#listOfUnits do
			tempGroup[listOfUnits[i]] --= k
--]]
	return groupArray  
end