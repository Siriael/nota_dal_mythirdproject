local sensorInfo = {
	name = "CreateGrid",
	desc = "Creates grid of points where every point is stored as key with his neighbours (in range) as values",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


local threshold = 50

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
-- @description return current wind statistics
return function(myUnitID,WantedPosition)
	local cmdID = CMD.MOVE
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(myUnitID[1])
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	if (pointmanPosition:Distance(WantedPosition) < threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(myUnitID[1], cmdID, WantedPosition:AsSpringVector(), {})
		return RUNNING
	end
end