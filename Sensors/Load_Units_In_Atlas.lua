local sensorInfo = {
	name = "loadUnits",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

return function( listOfUnits,dest,radius)
	
	
		local transporterID = listOfUnits[1]
		local cmdID = CMD.LOAD_UNITS
		SpringGiveOrderToUnit(transporterID, cmdID, {dest.x,dest.y,dest.z, radius},  {}) --- load units into bear 
		--[[
		
		Spring.GiveOrderToUnit(
    	transporterID,
		CMD.LOAD_UNITS
		{dest.x,dest.y,dest.z,radius}		
    	    	
		)--]]
	if (#Spring.GetUnitIsTransporting(transporterID) ~= 1) then
		return RUNNING
	else 
		return SUCCESS
	end
end