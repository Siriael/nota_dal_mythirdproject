local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function Distance(node_1,node_2)
	dist = math.sqrt (  (node_1.x - node_2.x ) * ( node_1.x - node_2.x ) + ( node_1.z - node_2.z )*( node_1.z - node_2.z) )
	Spring.Echo("dist is " , dist)
return dist
end



-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitIDs] unfiltered list 

-- @return groupArrays [table] filtered list
return function(listOfUnits,mAliveGroups )

	--[[numberOfGroups = 4 
	local groupSize = math.floor(#listOfUnits / numberOfGroups)
	if(groupSize<1)then
		groupSize = 1
	end--]]
	local groupArray = {}
	local missInfo = Sensors.core.MissionInfo()
	--local j = 1
	local i = 0
	
	for index,UnitId in pairs (listOfUnits) do
	
					
					if(i>=mAliveGroups)then
						i = 1
					else
						i= i +1
					end
					
					
					local temp = groupArray[i]
					if(temp == nil)then
						temp = {}
						temp[1] = UnitId
					else
						temp[#temp + 1] = UnitId
					end
					
					groupArray[i] = temp

	end
	
	--[[
	for j = 1, numberOfGroups do
		local tempGroup = {}
		local k = 1
		for  k = 1,groupSize do	

				local pos = Sensors.nota_DaL_myThirdProject.myPosition(listOfUnits[i])
				if (pos:Distance(missInfo.safeArea.center)>missInfo.safeArea.radius) then
					tempGroup[#tempGroup + 1] = listOfUnits[i]
				
					k = k+1
					
				end
				i = i + 1 
			
		end
		groupArray[j] = tempGroup  
		j = j +1 
	end
		--[[
		local tempGroup = {}
		local k = 1
		for i = 1,#listOfUnits do
			tempGroup[listOfUnits[i]] --= k
			
		
	local Task = { SavedUnits = {} , DeadUnits = {} , UnitsToRescue = groupArray , AliveGroups = mAliveGroups}
	return Task
end