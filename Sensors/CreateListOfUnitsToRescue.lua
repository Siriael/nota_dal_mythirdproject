local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


local function Distance(node_1,node_2)
	dist = math.sqrt (  (node_1.x - node_2.x ) * ( node_1.x - node_2.x ) + ( node_1.z - node_2.z )*( node_1.z - node_2.z) )
	
return dist
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
-- @argument unitID number
-- @description return static position of the first unit
return function(listOfUnits)

	local center = 
	for index,unitID in pairs(listOfUnits) do 
		
	end
	local number = unitID
	local x,y,z = SpringGetUnitPosition(number)
	return Vec3(x,y,z)
end