local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back





-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitID] unfiltered list 

-- @return groupArrays [table] filtered list

return function(atlasID  )
	
	if(bb[atlasID] == nil)then
		myTable = { PathIndex = 1 , UnitStucked = 0 ,StuckedTime = 0 , OrderGiven = false , path = {} }
		bb[atlasID] = myTable
	else
		myTable = bb[atlasID]
	end
	
	return 
end
