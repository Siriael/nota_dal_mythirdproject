local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return current wind statistics
-- @argument listOfUnits [array of unitIDS] unfiltered list
return function(listOfUnits,pos,dest )
		
		
		local thisUnitDefID
		local thisUnitID
		local myres
		for i=1, #listOfUnits do
		
			
			thisUnitID = listOfUnits[i]
			
			--Spring.Echo(thisUnitID)
			if(thisUnitID ~= nil) then
			thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
			--local unitDef = UnitDefs[unitDefID]

			local myres = UnitDefs[thisUnitDefID].moveDef.id
			Spring.Echo(myres)
			end
			
			-- moveID = UnitDefs[thisUnitDefID].moveDef
			--[[if (category[thisUnitDefID] ~= nil) then -- in category
				newListOfUnits[#newListOfUnits + 1] = thisUnitID
			end --]]
			

			
		end
		
	  local path = Spring.RequestPath(0,pos.x,pos.y,pos.z, dest.x,dest.y,dest.z)
	  Spring.Echo(path)
	return path.GetPathWayPoints()
	--return myres
	
	
end
